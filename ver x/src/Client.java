import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

	Client() throws UnknownHostException, IOException {
		Socket socketClient= new Socket("localhost",11111);
		System.out.println("Connected to server");
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
	    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socketClient.getOutputStream()));
	    
	    GUI gui = new GUI();
	    gui.perform(reader, writer);	        
	}
	
	
}
