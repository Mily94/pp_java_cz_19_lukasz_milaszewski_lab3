import java.io.IOException;
import java.util.Scanner;

class Main {
   public static void main (String [] args) throws IOException{
	   Scanner reader = new Scanner(System.in);
	   
	   System.out.println("0 - run server.");
	   System.out.println("1 - run client.");
	   int n = reader.nextInt();
	   
	   if (n == 0) {
		   Server server = new Server();
	   }
	   
	   if (n == 1) {
		   Client client = new Client();
	   }
   }
}