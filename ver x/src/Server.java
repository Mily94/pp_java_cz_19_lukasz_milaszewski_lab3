import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	static int iter = 0;

	ServerSocket myServerSocket;
	
	public Server() throws IOException { 
		myServerSocket = new ServerSocket(11111); 
		System.out.println("Server is running");
		while(true) {
			Socket clientSocket = myServerSocket.accept();
			ClientThread cliThread = new ClientThread(clientSocket);
			cliThread.start();
		}
	}
}
