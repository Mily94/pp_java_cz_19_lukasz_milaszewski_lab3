
public class Logic {
	
    static String getPlayerCards(Player player) {
        String cards = "";
        for (int i = 0; i < player.getSize(); i++) { 
            cards += player.getValue(i);
            cards += " ";
        }   
        return cards;
    }
    
    static void hit(Player player) {
        Card card = new Card();
        player.addCard(card);
    }
    
    static void performCroupierMoves(Player croupier) {
    	while(getSum(croupier) < 16) {
    		hit(croupier);
    	}
    }
    
    static int getSum(Player player) {
    	int sum = 0;
    	for (int i = 0; i < player.getSize(); i++) 
    		sum += player.getValue(i);
    	
    	return sum;
    }
    
    static boolean ifHumanWon(Player human, Player croupier) {
    	if (getSum(human) > 21)
    		return false;
    	
    	if (getSum(croupier) > 21)
    		return true;
    	
    	if (getSum(human) > getSum(croupier) )
    		return true;
    	return false;
    }  
}
