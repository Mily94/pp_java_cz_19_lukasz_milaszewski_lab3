import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;

public class ClientThread extends Thread {
	Socket clientSocket;
	boolean runThread = true;
	
	ClientThread(Socket s) {
		clientSocket = s;
	}
	
	public void run() {
		 BufferedReader reader = null; 
         PrintWriter writer = null; 
         Player human = new Player();
         Player croupier = new Player();
         Server.iter++;
         int iter = Server.iter;
         System.out.println("Client" + iter + " connected.");
         
         try {
			reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			writer = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
			while(runThread) {
				
				// wyslanie startowych kart gracza
				writer.write(Logic.getPlayerCards(human)+ "\r\n");
				writer.flush();
				
				// wyslanie karty startowej krupiera
				writer.write(croupier.getValue(0) + "\r\n");
				writer.flush();
				
				String temp = reader.readLine();
				
				while(!temp.equals("close")) {
					//System.out.println(temp);
					Logic.hit(human);
					writer.write(Logic.getPlayerCards(human) + "\r\n");
					writer.flush();
					temp = reader.readLine();
				} 
				
				Logic.performCroupierMoves(croupier);
				writer.write(Logic.getPlayerCards(croupier) + "\r\n");
				writer.flush();
				
				boolean humanWon = Logic.ifHumanWon(human, croupier);
				writer.write(humanWon + "\r\n");
				writer.flush();
				
				runThread = false;
			}
         
         } catch (IOException e) {
			e.printStackTrace();
		 }    
         finally { 
             try {     
                 reader.close(); 
                 writer.close(); 
                 clientSocket.close(); 
                 System.out.println("Client" + iter + " disconnected."); 
             } 
             catch(IOException ioe) { 
                 ioe.printStackTrace(); 
             } 
         } 
	}
}
