import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GUI {
    public void perform(Player human) throws UnknownHostException, IOException {
    
       	Socket socketClient= new Socket("localhost",5555);
	    System.out.println("Client: "+"Connection Established");

	    BufferedReader reader = 
	    		new BufferedReader(new InputStreamReader(socketClient.getInputStream()));

	    BufferedWriter writer= 
        		new BufferedWriter(new OutputStreamWriter(socketClient.getOutputStream()));
	
    	
	    String croupierCard = reader.readLine();
	    System.out.println("Received: croupierCard = " + croupierCard);
	    
	    JFrame window = setFrame();
        JPanel panel = setPanel(window);
        
        JPanel subPanelSouth = setSubPanel();
        JPanel subPanelNorth = setSubPanel();
        JPanel subPanelEast = setSubPanel();
        JPanel subPanelWest = setSubPanel();
        
        JLabel labelNorth = new JLabel();
        JLabel labelEast = new JLabel();
        JLabel labelWest = new JLabel();
        JLabel betLabel = new JLabel();
        JLabel humanCards = new JLabel();
        JLabel croupierCards = new JLabel();
        
        // NORTH
        addToSubPanel(subPanelNorth, setLabel("Your cards:                                ", labelEast));
        addToSubPanel(subPanelNorth, setLabel("                           Croupier cards: ", labelWest));
        
        // EAST
        addToSubPanel(subPanelEast, setLabel(" ", croupierCards));
        croupierCards.setText(croupierCard);
        
        // WEST
        addToSubPanel(subPanelWest, setLabel(" ", humanCards));
        humanCards.setText(Printer.getPlayerCards(human));
        
        // SOUTH
        JButton hitButton = setHitButton(human, humanCards);
        JButton stopButton = setStopButton(human, croupierCards, writer, reader);
        addToSubPanel(subPanelSouth, hitButton);
        addToSubPanel(subPanelSouth, stopButton);
        
        panel.add(subPanelNorth, BorderLayout.NORTH);
        panel.add(subPanelEast, BorderLayout.EAST);
        panel.add(subPanelWest, BorderLayout.WEST);
        panel.add(subPanelSouth, BorderLayout.SOUTH);
    }

    void addToSubPanel(JPanel subPanel, JLabel label) {
        subPanel.add(label);
    }
    
    void addToSubPanel(JPanel subPanel, JTextField textField) {
        subPanel.add(textField);
    }
    
    void addToSubPanel(JPanel subPanel, JButton button) {
        subPanel.add(button);
    }
    
    JLabel setLabel(String text, JLabel label) {
        label.setText(text);
        return label;
    }
    
    JPanel setSubPanel() {
        return new JPanel();
    }
    
    JFrame setFrame() {
        JFrame window = new JFrame("Black Jack");
        window.setLocationRelativeTo(null);
        window.setVisible(true);
        window.setSize(450,150);
        window.setResizable(false);
        
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        return window;
    }
    
    JPanel setPanel(JFrame window) {
        JPanel panel = new JPanel(new BorderLayout());
        window.add(panel);
        return panel;
    }
    
    JButton setHitButton(Player human, JLabel humanCards) {
        JButton hitButton = new JButton("Hit");
        hitButton.setVisible(true);
        
        hitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Logic.hit(human);
                humanCards.setText(Printer.getPlayerCards(human));    
            }
        });
        return hitButton;
    }
      
    JButton setStopButton(Player human, JLabel croupierLabel, BufferedWriter writer, BufferedReader reader) {
    	JButton stopButton = new JButton("Stop");
    	stopButton.setVisible(true);
  
    	stopButton.addActionListener(new ActionListener() {
    		@Override
    		public void actionPerformed(ActionEvent e) {
    			try {
					writer.write(Logic.getSum(human)+"\r\n");
					writer.flush();
					System.out.println("Sent: humanSum = " + Logic.getSum(human));
					
					String croupierCards = reader.readLine();
					croupierLabel.setText(croupierCards);
					System.out.println("Received: croupierCards = " + croupierCards);
					
					String croupierSum = reader.readLine();
					System.out.println("Received: croupier Sum = " + croupierSum);
					
					
					String whoWon = reader.readLine();
					System.out.println("Received: whoWon = " + whoWon);
					
					String playerSum = Integer.toString(Logic.getSum(human));
					
					if (Integer.parseInt(whoWon) == 1)
						whoWon = "gracz";
					else
						whoWon = "krupier";
					showMessage(playerSum, croupierSum, whoWon);
				
    			} catch (IOException e1) {
					e1.printStackTrace();
				}
    		}
    	});
    	return stopButton;
    } 
    
    void showMessage(String playerCards, String croupierCards, String whoWon) {
        
    	String msg = "Twoja suma kart: " + playerCards +
    			"\nSuma kart krupiera: " + croupierCards +
    			"\nWygra�: " +whoWon;
        JOptionPane optionPane = new JOptionPane();
        optionPane.setMessage(msg);
        optionPane.setMessageType(JOptionPane.INFORMATION_MESSAGE);
        JDialog dialog = optionPane.createDialog(null, "Wynik");
        dialog.setVisible(true);
        System.exit(0);    
      }
} 