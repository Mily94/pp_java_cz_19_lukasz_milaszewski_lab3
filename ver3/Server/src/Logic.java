
public class Logic {
    
    static void hit(Player player) {
        Card card = new Card();
        player.addCard(card);
    }
    
    static void performCroupierMoves(Player croupier) {
    	while(getSum(croupier) < 16) {
    		hit(croupier);
    	}
    }
    
    static int getSum(Player player) {
    	int sum = 0;
    	for (int i = 0; i < player.getSize(); i++) 
    		sum += player.getValue(i);
    	
    	return sum;
    }
    
    static int ifHumanWon(int humanSum, int croupierSum) {
    	if (humanSum > 21)
    		return 0;
    	
    	if (croupierSum > 21)
    		return 1;
    	
    	if (humanSum > croupierSum)
    		return 1;
    	return 0;
    }
    
    static String getPlayerCards(Player player) {
        String cards = "";
        for (int i = 0; i < player.getSize(); i++) { 
            cards += player.getValue(i);
            cards += " ";
        }   
        cards += "\r\n";
        return cards;
    }
}
