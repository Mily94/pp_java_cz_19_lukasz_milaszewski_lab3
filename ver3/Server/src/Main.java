import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {
	
	public static void main(String[] args) throws IOException {
		ServerSocket mysocket = new ServerSocket(5555);
		
		System.out.println("Server is running");
		
		while(true) {
			Socket connectionSocket = mysocket.accept();
			
			BufferedReader reader =
					new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
	        BufferedWriter writer= 
	           		new BufferedWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));
	        System.out.println("--------------------------------");
	        Player croupier = new Player();
	        writer.write(croupier.getValue(0) + "\r\n");
	        writer.flush();
	        System.out.println("Sent: croupierCard = " + croupier.getValue(0));
	        
	        String humanSum = reader.readLine().trim();
	        System.out.println("Received: humanSum = " + humanSum);
	        Logic.performCroupierMoves(croupier);
	           
	        String croupierCards = Logic.getPlayerCards(croupier);
	        writer.write(croupierCards);
	        writer.flush();
	        System.out.println("Sent: croupierCards = " + croupierCards);
	        
	        int croupierSum = Logic.getSum(croupier);
	        
	        writer.write(croupierSum + "\r\n");
	        writer.flush();
	        System.out.println("Sent: croupierSum = " + croupierSum);
	        
	        int whoWon = Logic.ifHumanWon(Integer.parseInt(humanSum), croupierSum);
	        writer.write(whoWon + "\r\n");
	        writer.flush();
	        System.out.println("Sent: whoWon = " + whoWon);
	        
	        connectionSocket.close();
		}
		
	}
}