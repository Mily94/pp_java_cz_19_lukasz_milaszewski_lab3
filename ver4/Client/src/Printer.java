
public class Printer {
    
    static String getPlayerCards(Player player) {
        String cards = "";
        for (int i = 0; i < player.getSize(); i++) { 
            cards += player.getValue(i);
            cards += " ";
        }   
        return cards;
    }
}
