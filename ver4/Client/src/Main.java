import java.io.IOException;
import java.net.UnknownHostException;

public class Main {
    
    public static void main (String [] args) throws UnknownHostException, IOException {
        Player human = new Player();
        GUI gui = new GUI();
        
        gui.perform(human);        
    }
}
