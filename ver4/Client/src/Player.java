import java.util.ArrayList;

public class Player {
   private ArrayList<Card> cards = new ArrayList<Card>();
   
    Player() {
        Card firstCard = new Card();
        Card secondCard = new Card();
        addCard(firstCard);
        addCard(secondCard);
    }
    
    void addCard(Card card) {
        cards.add(card);
    }

    int getSize() {
        return cards.size();
    }
    
    int getValue(int i) {
        return cards.get(i).getValue();
    }
}
