import java.util.Random;

public class Card {
    private int value;
    
    Card () {
    	Random random = new Random();
    	int randomNumber = random.nextInt(11 + 1 - 2) + 2;
        this.value = randomNumber;
    }
    
    void setValue(int value) {
        this.value = value;
    }
    
    int getValue() {
        return value;
    }
}
