
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    ServerSocket myServerSocket;
    static int iter = 0;

    public Server() { 
        try { 
            myServerSocket = new ServerSocket(11111); 
        } 
        catch(IOException ioe) { 
            System.out.println("Could not create server socket on port 11111. Quitting."); 
            System.exit(-1); 
        } 
        
        System.out.println("Server is running");
 
        while(true) {                        
            try  { 
                Socket clientSocket = myServerSocket.accept(); 

                ClientServiceThread cliThread = new ClientServiceThread(clientSocket);
                cliThread.start(); 

            } 
            catch(IOException ioe) { 
                System.out.println("Exception encountered on accept. Ignoring. Stack Trace :"); 
                ioe.printStackTrace(); 
            } 

        }
    }
}