import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

class ClientServiceThread extends Thread { 
        Socket myClientSocket;
        boolean m_bRunThread = true; 

      /*  public ClientServiceThread() { 
            super(); 
        }*/ 

        ClientServiceThread(Socket s) { 
            myClientSocket = s; 
        } 

        public void run() {            
            BufferedReader reader = null; 
            PrintWriter writer = null; 
            Server.iter++;
            int iter = Server.iter;
            System.out.println("Accepted Client" + Server.iter + " Address - " + myClientSocket.getInetAddress().getHostName()); 

            try  {                                
                reader = new BufferedReader(new InputStreamReader(myClientSocket.getInputStream())); 
                writer = new PrintWriter(new OutputStreamWriter(myClientSocket.getOutputStream())); 

                while(m_bRunThread) {            
                    
                	Player croupier = new Player();
        			int croupierCard = croupier.getValue(0);
        			
        			writer.write(croupierCard + "\r\n");
        	        writer.flush();
        	        System.out.println("Sent: croupierCard = " + croupierCard);
        	        String humanSum = reader.readLine().trim();
        	        System.out.println("-------------------------------");
        	        System.out.println("Received: humanSum = " + humanSum);
        	        Logic.performCroupierMoves(croupier);
        	           
        	        String croupierCards = Logic.getPlayerCards(croupier);
        	        writer.write(croupierCards);
        	        writer.flush();
        	        System.out.println("Sent: croupierCards = " + croupierCards);
        	        
        	        int croupierSum = Logic.getSum(croupier);
        	        
        	        writer.write(croupierSum + "\r\n");
        	        writer.flush();
        	        System.out.println("Sent: croupierSum = " + croupierSum);
        	        
        	        int whoWon = Logic.ifHumanWon(Integer.parseInt(humanSum), croupierSum);
        	        writer.write(whoWon + "\r\n");
        	        writer.flush();
        	        System.out.println("Sent: whoWon = " + whoWon);
        	       
        	        m_bRunThread = false;      
                } 
            } 
            catch(Exception e)  { 
                e.printStackTrace(); 
            } 
            finally { 
                try {     
                    reader.close(); 
                    writer.close(); 
                    myClientSocket.close(); 
                    System.out.println("Client" + iter +  " disconnected"); 
                } 
                catch(IOException ioe) { 
                    ioe.printStackTrace(); 
                } 
            } 
        } 
    }